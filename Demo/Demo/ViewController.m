//
//  ViewController.m
//  Demo
//
//  Created by Rakesh Kumar on 2/28/15.
//  Copyright (c) 2015 Click Labs. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton  *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 100, 100);
    [self.view addSubview:button];
    
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(100, 100, 200, 200)];
    [self.view addSubview:imageView];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 20, 20)];
    label.text = @"Label";
    [self.view addSubview:label];
    
    UIView *view = [[UIView alloc]init];
    [self.view addSubview:view];
    
    UITableView *tableView = [[UITableView alloc]init];
    [self.view addSubview:tableView];
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
